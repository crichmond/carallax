$( document ).ready( function() {
	$( ".no-js" ).removeClass( "no-js" );

	$("#carallax_main").carallax( {
		selectors : {
			title : ".clx_title",
			img : ".clx_img",
			body : ".clx_body",
			link : ".clx_link",
			info : ".clx_info",
			slide : ".clx_slide"
		},
		transitionSpeed : 250,
		transitionType : "fade",
		multidimensional : true,
		parallaxFactor : 2, // values >= 1 trigger parallaxing
		height : "full",
		slide : 0,
		auto: true, // makes slideshow automatically advance
		autoAdvanceDelay : 5000, // time spent on each slide when auto == true
		autoResumeDelay : 8000 // delay before auto bevahior resumes after the slideshow is manually jogged
	} );

	$("#carallax_sub").carallax( {
		selectors : {
			title : ".clx_title",
			img : ".clx_img",
			body : ".clx_body",
			link : ".clx_link",
			info : ".clx_info",
			slide : ".clx_slide"
		},
		transitionSpeed : 500,
		transitionType : "slide",
		multidimensional : false,
		parallaxFactor : 0, // 0 turns off parallaxing
		height : 600, // arbitrary px value -- can be anything
		slide : 0
	} );
} );