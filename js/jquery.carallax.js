/*
* @fileOverview Carallax - jQuery Plugin
* @version 0.3
*
* @author Cody Richmond / Vector Bridge (http://vectorbridge.com)
*
* @dependencies: 
*	jQuery (1.11.0)
*	TouchSwipe (1.6.5)
*	Underscore (1.5.2)
*	Modernizr (2.7.1)ƒ
*/


( function( $ ) {
	"use strict";


	//----------------------------------------------------
	// CARALLAX OBJ
	//----------------------------------------------------
	var Carallax = function( el, opts ){
		var self = this,

			//----------------------------------------------------
			// CONSTANTS
			//----------------------------------------------------
			PERCENT = "PERCENT",
			PIXEL = "PIXEL",
			FULL = "FULL",


			//----------------------------------------------------
			// DEPENDENCY CHECKING
			//----------------------------------------------------
			Modernizr = window.Modernizr || false, // Modernizr 2.7.1
			_ = window._ || false, // Underscore 1.5.2
			console = window.console || {},
			out = console ? ( console.log ? function() { console.log( arguments ); } : false ) : false || function() {},

			//----------------------------------------------------
			// SELECTOR CACHE
			//----------------------------------------------------
			$el = $( el ),
			$HTML = $( "html" ),
			$BODY = $( "body" ),
			$WIN = $( window ),
			$LOADER = $(),
			$SLIDES = $(),
			$SLIDE_MOVER = $(),
			$ACTIVE_SLIDES = $(),
			$ACTIVE_INFOS = $(),
			$ARROW_L = $(),
			$ARROW_R = $(),
			$CONTROLS = $(),

			//----------------------------------------------------
			// MISC VARS
			//----------------------------------------------------
			curSlide = 0,
			totalSlides = 0,
			isTransitioning = false,
			slideProps = {
				slideWidth : 0,
				totalWidth : 0
			},
			slideTout = 0,
			autoAdvanceInt = 0,
			autoAdvanceResumeTout = 0,
			classes = { // class name reference for our elements
				isLoaded : "clx__isLoaded",
				loader : "clx__loader",
				outer : "clx__outer",
				title : "clx__title",
				img : "clx__img",
				body : "clx__body",
				info : "clx__info",
				slide : "clx__slide",
				slideMover : "clx__slideMover",
				break : "clx__break",
				link : "clx__link",
				arrow : "clx__arrow",
				chooser : "clx__chooser",
				chooserOuter : "clx__chooserOuter",
				bgLayer1 : "clx__bgLayer1",
				bgLayer2 : "clx__bgLayer2",
				bgLayer3 : "clx__bgLayer3"
			},
			defaults = {
				height : "full", // or # (ex: 100 or "100px") or % (ex: "50%")
				preloader : true,
				parallaxFactor : 2, // x == 0 disables parallax; x >= 1 induces parallax
				multidimensional : true,
				slide : 0,
				transitionSpeed : 200,
				transitionType : "fade", // "fade|slide"
				auto : false, // makes slideshow automatically advance
				autoAdvanceDelay : 5000, // time spent on each slide when auto == true
				autoResumeDelay : 7000, // delay before auto bevahior resumes after the slideshow is manually jogged
				custom : false, // toggle for custom treatment for Exponential subpages
				selectors : {
					// title : ".classOfTitles", 
					// image : ".classOfImages", 
					// body : ".classOfBodyCopy", 
					// link : ".classOfLink", 
					// slide : ".classOfSlide"
				}
			},
			settings = $.extend( defaults, opts || {} ),

			// -------------------------------------------
			// SELF INITIALIZATION
			// -------------------------------------------
			assignClasses = function() {
				// loop through all provided selectors and mate them to our proprietary classes
				var sel;
				for( sel in settings.selectors ) {
					if( !!settings.selectors[ sel ] && !!classes[ sel ] ) {
						$el.find( settings.selectors[ sel ] ).addClass( classes[ sel ] ); // add our proprietary classes to the supplied identifier classes
					}
				}
			},
			injectPreloader = function() {
				if( settings.preloader === true ) {
					$LOADER = $( "<aside class = \"" + classes.loader + "\"></aside>" );
					$el.append( $LOADER );
				}
			},
			actOnLoaded = function() {
				
				$el.closest( "." + classes.outer ).addClass( classes.isLoaded );
				if( $LOADER && $LOADER.length )
					$LOADER.remove();
			},
			wrapContainer = function() {
				// provide plugin-jailing wrapper
				var oldContent = "";
				if( settings.transitionType === "slide" ) {
					oldContent = $el.html();
					$el.html( "<div class = \"" + classes.slideMover + "\">" + oldContent + "</div>" );
					$SLIDE_MOVER = $el.find( "." + classes.slideMover );
				}
				$el.wrap( "<div class = \"" + classes.outer + "\" />" );
			},
			defineSlides = function() {
				$SLIDES = $el.find( "." + classes.slide );
			},
			getHeight = function() {
				// return height in appropriate units
				var h;
				switch( settings.heightType ) {
					case PERCENT :
						h = Math.ceil( settings.height * $WIN.height() ) + "px";
						break;
					case PIXEL :
						h = settings.height + "px";
						break;
					case FULL :
					default :
						h = $WIN.height() + "px";
						break;
				}
				return h;
			},
			cleanHeight = function() {
				// parse height to something useful
				var hType = typeof settings.height,
					hParsed = Math.ceil( parseInt( settings.height ) );
				
				if( ( hType === "string" && settings.height.indexOf( "px" ) > -1 && hParsed > 0 )
					|| hType !== "string" ) {
					// PIXEL height (string or number)
					settings.heightType = PIXEL;
					settings.height = hParsed;
				} else if( hType === "string" ) {
					// NON-PIXEL height
					if( settings.height.indexOf( "%") > -1 && hParsed > 0 ) {
						// PERCENT height
						settings.heightType = PERCENT;
						settings.height = hParsed / 100;
					} else {
						// FULL height (basically any string we don't identify as px or %)
						settings.heightType = FULL;
					}
				}
			},
			assignHeight = function() {
				// set height of carallax
				var $outer = $el.closest( "." + classes.outer );

				$outer.css( {
					height : getHeight()
				});

			},
			watchForResize = function() {
				// keep an eye on resize events
				// reposition captions and resize carallax accordingly
				var rearrange = function() {
						repositionInfo();
						assignHeight();
						if( settings.transitionType === "slide" )
							onResizeTransition_slide();
					},
					resizeHandler = _.debounce( rearrange, 50 );

				$WIN.on( "resize", resizeHandler );

				rearrange();
			},
			repositionInfo = function() {
				// vertically center info (title/body/link) in slide
				var $infos = $SLIDES.find( "." + classes.info ),
					slideH = $SLIDES.eq( 0 ).height();

				$infos.each( function() {
					var $this = $( this ),
						thisH = $this.height();

					$this.css( {
						"top" : ( slideH - thisH ) / 2 + "px"
					} );
				} );
			},
			transferImages = function() {
				// take images (img elements) and set them to background images of slides
				$SLIDES.each( function() {
					var $slide = $( this ),
						$img = $slide.find( "." + classes.img );

					if( settings.transitionType === "slide" ) {
						$img.parent().css( {
							"background-image" : "url(" + $img.attr( "src" ) + ")"
						} );
						drawBlur( $img, $slide.find( "." + classes.bgLayer3 ) );
					} else {
						$slide.css( {
							"background-image" : "url(" + $img.attr( "src" ) + ")"
						} );
					}
					$img.remove();
				} );

				totalSlides = $SLIDES.length;
			},
			drawBlur = function( $img, $tgt ) {
				var revealCanvas = function( $canvas ) {
						$canvas.parent().addClass( "processed" );
					},
					recenterBannerImage = function( $canvas, origW, origH ) {
						// debugger;
			 			var canvasW = 0,
					 		canvasH = 0,
					 		wrapW = $el.width(),
					 		wrapH = $el.height(),
					 		imgRatio = origW / origH,
					 		wrapRatio = wrapW / wrapH,
					 		offset = 0;
					 		// debugger;
					 	// 1 = square; >1 = long; <1 = tall
					 	if( imgRatio > wrapRatio ) {
					 		// img/canvas is longer than wrap
					 		offset = wrapH * imgRatio;
					 		$canvas.css( { 
					 			"width" : offset + "px",
					 			"height" : "100%",
					 			"top" : "0",
					 			"left" : ( ( wrapW - offset ) / 2 ) + "px"
					 		} );
					 		
					 	} else {
					 		// img/canvas is taller than wrap
					 		offset = wrapW * imgRatio;
							$canvas.css( { 
					 			"width" : "100%",
					 			"height" : offset + "px",
					 			"top" : ( ( wrapH - offset ) / 2 ) + "px",
					 			"left" : "0"
					 		} );
					 	}

					 	
			 		};

		 		var canvasId = "vb-util_blur-canvas" + _.uniqueId(),
		 			canvasClass = "vb-util_blur-canvas",
		 			imgId = "vb-util_blur-img" + _.uniqueId(),
		 			blurRad = 15,
		 			imgW = 0,
		 			imgH = 0,
		 			$img1,
		 			$canvas;	
		 		
			 	if( $img.length && $tgt.length ) {
			 		$img1 = $img.eq( 0 );

			 		$canvas = $( "<canvas id = \"" + canvasId + "\" class = \"" + canvasClass + "\" />" );
			 		$tgt.prepend( $canvas );

			 		$img1.attr( "id", imgId );

			 		stackBlurImage( imgId, canvasId, blurRad );
			 		
			 		imgW = $img1.naturalWidth();
			 		imgH = $img1.naturalHeight();

			 		$WIN.on( "resize", _.debounce( function() {
			 			recenterBannerImage( $canvas, imgW, imgH );
			 		}, 50 ) );
			 		recenterBannerImage( $canvas, imgW, imgH );
			 	} else {
			 		out( "Warning: No image or target available for drawBlur()" );
			 	}

			},
			addSpace = function() {
				// add breaking elements between title/body/link elements
				// this allows there to be any combination of the three without losing space integrity to float issues
				var $els = $el.find( "." + classes.title + ", ." + classes.body );

				$els.after( "<br class = \"" + classes.break + "\" />" );
			},

			// -------------------------------------------
			// PARALLAX
			// -------------------------------------------
			initParallax = function() {
				// if not a touch device and does allow CSS transforms...
				if( !Modernizr.touch && $HTML.hasClass( "csstransforms" ) ) {
					// determine which slides should be parallaxed
					assignParallaxVars();
					// and start watching for scroll events to perform the magic
					attachParallax();
				}
			},
			attachParallax = function() {
				// attach scroll handler which will execute parallax logic
				$WIN
					.off( "scroll", performParallax )
					.on( "scroll", performParallax );
				performParallax();
			},
			performParallax = function() {
				// determine how much parallaxing to do
				var realY = window.scrollY > 0 ? window.scrollY : 0, // >=0 value for parallax offset
					newY = realY / settings.parallaxFactor; // divide by slow setting to create parallax

				// apply offset to slide bg
				offset( $ACTIVE_SLIDES, newY, "y" );
				// if we're offsetting infos as well...
				if( settings.multidimensional ) {
					// ... apply offset to infos
					offset( $ACTIVE_INFOS, -newY/4, "y" );
				}
			},
			assignParallaxVars = function() {
				// only perform parallax on visible elements; set global vars for these
				$ACTIVE_SLIDES = $SLIDES.filter( ":visible" );
				$ACTIVE_INFOS = $ACTIVE_SLIDES.find( "." + classes.info );

				// make sure we're working with a valid settings.parallaxFactor value
				if( typeof settings.parallaxFactor !== "number" || settings.parallaxFactor < 0 ) {
					settings.parallaxFactor = 0; // turn off parallax if < 0 or NaN
				} else if( settings.parallaxFactor > 0 && settings.parallaxFactor < 1 ) {
					settings.parallaxFactor = Math.round( settings.parallaxFactor );
				}

			},
			offset = function( $tgt, amt, xOrY ) {
				// actually perform parallax offsetting
				xOrY = ( xOrY || "x" ).toUpperCase();
				amt = Math.floor( amt ); // don't allow decimals 
				$tgt.css( {
					"-webkit-transform" : "translate" + xOrY + "(" + amt + "px)",
					"-ms-transform" : "translate" + xOrY + "(" + amt + "px)",
					"transform" : "translate" + xOrY + "(" + amt + "px)"
				} );
			},


			// -------------------------------------------
			// SLIDE-TRANSITION STUFF
			// -------------------------------------------
			addBgLayers = function() {
				if( settings.transitionType !== "slide" )
					return false;

				$SLIDES.each( function() {
					var $slide = $( this ),
						$layer1 = $( "<div class = \"" + classes.bgLayer1 + "\"/>" ),
						$layer2 = $( "<div class = \"" + classes.bgLayer2 + "\"/>" ),
						$layer3 = $( "<div class = \"" + classes.bgLayer3 + "\"/>" );
					$slide
						.prepend( $layer3 )
						.prepend( $layer2 )
						.prepend( $layer1 );
				} );
			},
			onResizeTransition_slide = function() {
				assignVarsTransition_slide();
				adjustSlidesTransition_slide();
				performSlideTransition( curSlide );
			},
			prepareForTransition_slide = function() {
				assignVarsTransition_slide();
				adjustSlidesTransition_slide();

				$SLIDES.show();

				repositionInfo();
				// debugger;
			},
			assignVarsTransition_slide = function() {
				var w = $el.width();
				slideProps = {
					slideWidth : w,
					totalWidth : w * $SLIDES.length
				}
			},
			adjustSlidesTransition_slide = function() {
				// debugger;
				$SLIDE_MOVER
					.css( { 
						"width" : slideProps.totalWidth + "px",
						"transition-duration" : settings.transitionSpeed / 1000 + "s",
						"-webkit-transition-duration" : settings.transitionSpeed / 1000 + "s"
					} );

				$SLIDES
					.css( { 
						"width" : slideProps.slideWidth + "px"
					} )
					.each( function( i, slide ) {
						var $slide = $( slide );
						$slide.css( {
							"left" : ( i * slideProps.slideWidth ) + "px"
						} );
					} );
				
			},
			performSlideTransition = function( which, force ) {
				// debugger;
				var newSlide = ( function() {
						if( which >= totalSlides ) {
							return 0;
						} else if( which < 0 ) {
							return totalSlides - 1;
						}
						return which;
					} )(),
					amtToMove = -slideProps.slideWidth * newSlide;

				clearTimeout( slideTout );

				isTransitioning = true;

				offset( $SLIDE_MOVER, amtToMove, "x" );

				slideTout = setTimeout( function() {
					isTransitioning = false;
				}, force ? 0 : settings.transitionSpeed );

				curSlide = newSlide;
				switchActiveControl();
			},
			
			// -------------------------------------------
			// FADE TRANSITION STUFF
			// -------------------------------------------

			performFadeTransition = function( which, force ) {
				var newSlide = ( function() {
						if( which >= totalSlides ) {
							return 0;
						} else if( which < 0 ) {
							return totalSlides - 1;
						}
						return which;
					} )(),
					$fadeIn = $SLIDES.eq( newSlide ),
					$fadeOut = $SLIDES.not( $fadeIn ),
					onComplete = function() {
						isTransitioning = false;
						$fadeOut.hide();

						initParallax();
					};

				isTransitioning = true;

				// fade move new slide to top and fade in
				$fadeIn
					.hide()
					.css( { "z-index" : 3 } )
					.fadeIn( force ? 0 : settings.transitionSpeed, onComplete );
				
				// reposition content
				repositionInfo();
				// begin parallax effect if we can
				initParallax();

				// place old slide beneath new slide
				$fadeOut
					.css( { "z-index" : 2 } );

				curSlide = newSlide;

				switchActiveControl();
			},

			
			// -------------------------------------------
			// CAROUSEL METHODS
			// -------------------------------------------
			initSlideshow = function() {
				// start on first slide
				switch( settings.transitionType ) {
					case "slide" :
						prepareForTransition_slide();
						break;
					case "fade" :
					default :
						// don't have to do anything special for the fade/default case at the moment
						break;
				}
				self.changeSlides( settings.slide, true );
				$el.show();
			},
			injectArrows = function() {
				// add forward/back arrow controls
				var arrowTemplate = "<a href = \"javascript:void(0);\" class = \"" + classes.arrow + " {{CLASSE}}\">{{ICON}}</a>",
					$makeArrow = function( classe, icon ) {
						return $( arrowTemplate.replace( "{{CLASSE}}", classe ).replace( "{{ICON}}", icon ) );
					};
	
				$ARROW_R = $makeArrow( "right", "&gt;" ).insertAfter( $el );
				$ARROW_L = $makeArrow( "left", "&lt;" ).insertAfter( $el );

				$ARROW_R.on( "click", self.nextSlide );
				$ARROW_L.on( "click", self.prevSlide );
			},
			injectControls = function() {
				// add slide selector controls
				var controlTemplate = "<li class = \"" + classes.chooser + "\">{{SLIDE}}</li>",
					$allControls = $(),
					$container = $( "<ul class = \"" + classes.chooserOuter + "\"></ul>" ),
					i = 0,
					$makeControl = function( slide ) {
						var $control = $( controlTemplate.replace( "{{SLIDE}}", slide ) );
						$control.on( "click", function() {
							self.changeSlides( slide );
						} );
						return $control;
					};

				while( i < totalSlides ) {
					var $control = $makeControl( i );
					$allControls = $allControls.add( $control );
					i++;
				}

				$container.append( $allControls ).insertAfter( $el );
				$CONTROLS = $container;
			},
			makeControlsFadeable = function() {
				// if not a touch device and not IE8 or older...
				if( !Modernizr.touch && !$BODY.hasClass( ".lt-ie9" ) ) {
					// ... then make the controls fade in/out based on cursor presence
					// delay initial fade-out so user can see controls
					setTimeout( function() {
						$el.closest( "." + classes.outer ).addClass( "no-touch" );
					}, 3000 );
				}
			},
			switchActiveControl = function() {
				// toggle correct active control
				$CONTROLS.find( "." + classes.chooser )
					.removeClass( "active" )
					.eq( curSlide )
						.addClass( "active" );
			},
			makeTouchEnabled = function() {
				// if using a touch device...
				if( !!Modernizr.touch ) {
					// ... enable swipe navigation
					$el.swipe( {
						swipeLeft : self.nextSlide,
						swipeRight : self.prevSlide,
						threshold : 50
					} );
				}
			},
			initializeAutoAdvance = function( hardStart ) {
				if( !settings.auto )
					return;

				clearInterval( autoAdvanceInt );
				clearTimeout( autoAdvanceResumeTout );

				autoAdvanceInt = setInterval( function() {
					self.changeSlides( curSlide + 1 );
				}, settings.autoAdvanceDelay );

				if( hardStart === true )
					self.changeSlides( curSlide + 1 );
			},
			pauseAutoAdvance = function() {
				if( !settings.auto )
					return;

				clearInterval( autoAdvanceInt );
				clearTimeout( autoAdvanceResumeTout );

				autoAdvanceResumeTout = setTimeout( function() {
					initializeAutoAdvance( true );
				}, settings.autoResumeDelay );
			};
			


		// -------------------------------------------
		// PUBLIC METHODS
		// -------------------------------------------
		self.changeSlides = function( which, force ) {
			if( !force && ( isTransitioning || which === curSlide ) ) {
				return false;
			}

			switch( settings.transitionType ) {
				case "slide" :
					performSlideTransition( which, force );
					break;
				case "fade" :
				default :
					performFadeTransition( which, force );
					break;
			}
			
			
		};

		self.nextSlide = function() {
			pauseAutoAdvance();
			self.changeSlides( curSlide + 1 );
		};
		self.prevSlide = function() {
			pauseAutoAdvance();
			self.changeSlides( curSlide - 1 );
		};


		// must have options defined, doesn't run without direction
		if( !opts ) {
			out( "No options defined for Carallax, failing out." );
			return;
		}

		// check for dependencies
		if( !Modernizr || !_ || typeof $.fn.swipe !== "function" ) {
			out( "Some dependencies not defined, failing out." );
			return;
		}

		// -------------------------------------------
		// EXECUTE SELF-INIT
		// -------------------------------------------

		assignClasses(); // assign proprietary CSS classes according to user-provided options
		wrapContainer(); // add plugin wrap to jail proprietary CSS selectors
		injectPreloader(); // add our preloader
		defineSlides(); // populate the $SLIDES var
		cleanHeight(); // parse user-provided height to something we can use
		watchForResize(); // watch for resize events and trigger repositioning/resizing

		// once all the images are loaded...
		$el.imagesLoaded( function() { 
			addBgLayers(); // add additional background layers if necessary
			transferImages(); // transfer our images to BG images for parallax
			addSpace(); // force space between text elements on slides
			actOnLoaded(); // perform any "onload" behavior we need

			// -------------------------------------------
			// EXECUTE SLIDESHOW
			// -------------------------------------------
			injectArrows(); // add forward/back arrow controls
			injectControls(); // add slide-selector controls
			initSlideshow(); // begin slideshow
			makeControlsFadeable(); // if the controls should respond to cursor presence, make it so
			makeTouchEnabled(); // if the slideshow should adapt to a touch device, make it so
			initializeAutoAdvance();
		} );
		
	};

	$.fn.carallax = function( opts ) {
		return this.each( function() {
			var $el = $( this ),
				carallax;

			// Return early if this element already has a carallax instance
			if( $el.data( "carallax" ) ) {
				return;
			}

			// pass options to carallax constructor
			carallax = new Carallax( this, opts );

			// Store carallax object in this element's data
			$el.data( "carallax", carallax );
		} );
	};
} )( jQuery );